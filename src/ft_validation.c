/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_validation.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgendry <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/08 11:23:09 by rgendry           #+#    #+#             */
/*   Updated: 2019/04/11 17:23:26 by rgendry          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <stdio.h>

int		ft_check(char *line)
{
	int i;
	int x;

	i = 0;
	x = 0;
	while ((line[i] >= '0' && line[i] <= '9') || line[i] == ' ' ||
			line[i] == '-')
	{
		if (i == 0 && line[i] == ' ')
			return (0);
		if (line[i] == ' ' && line[i + 1] == ' ' && line[i + 2] == ' ')
			return (0);
		if ((line[i] >= '0' && line[i] <= '9') && (i == 0 || line[i - 1] == ' '
					|| line[i - 1] == '-'))
			x++;
		i++;
	}
	return (x);
}

t_point	**ft_create(int size_x, int size_y)
{
	int		i;
	t_point	**map;

	i = 0;
	map = (t_point**)malloc(sizeof(t_point) * size_x);
	while (i < size_y)
	{
		map[i] = (t_point*)malloc(sizeof(t_point) * size_y);
		i++;
	}
	return (map);
}

void	ft_fill(char *line, int size_x, int y, t_point **map)
{
	int x;

	x = 0;
	while (x < size_x)
	{
		map[y][x].x = x;
		//printf("%d ", map[y][x].x);
		map[y][x].y = y;
		//printf("%d ", map[y][x].y);
		map[y][x].z = ft_atoi(line);
		//printf("%d    ", map[y][x].z);
		//map[x][y].color = 
		while (*line && (*line == '-' || (*line >= '0' && *line <= '9')))
			line++;
		while (*line && *line == ' ')
			line++;
		x++;
	}
	//printf("%s\n", " ");
}

int		ft_read(char *file, int size_x, int size_y, t_point **map)
{
	int		n;
	int		y;
	int		fd;
	char	*line;

	n = 0;
	y = 0;
	if ((fd = open(file, O_RDONLY)) == -1)
		return (0);
	while (get_next_line(fd, &line) && y <= size_y)
	{
		ft_fill(line, size_x, y, map);
		y++;
	}
	if (y == size_y)
	{
		ft_pixelput(map, size_x, size_y);
		return (1);
	}
	return (0);
}

int		ft_validation(char *file)
{
	int		size_x;
	int		old_x;
	int		size_y;
	int		fd;
	char	*line;

	old_x = 0;
	size_y = 0;
	if ((fd = open(file, O_RDONLY)) == -1)
		return (0);
	while (get_next_line(fd, &line))
	{
		if ((size_x = ft_check(line)) == 0)
		{
			//printf("%s", "Error");
			return (0);
		}
		if (old_x != 0 && old_x != size_x)
			return (0);
		old_x = size_x;
		size_y++;
	}
	close(fd);
	ft_read(file, size_x, size_y, ft_create(size_x, size_y));
	return (1);
}
