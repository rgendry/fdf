/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_graph.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgendry <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/08 14:28:13 by rgendry           #+#    #+#             */
/*   Updated: 2019/04/22 12:51:14 by rgendry          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_draw_line(int x0, int x1, int y0, int y1, void *mlx_ptr, void *win_ptr)
{
	int delta_x;
	int delta_y;
	int error;
	int delta_error;
	int y;
	int x;
	int dir_y;

	delta_x = x1 - x0;
	delta_y = y1 - y0;
	error = 0;
	delta_error = delta_y;
	y = y0;
	x = x0;
	dir_y = y1 - y0;
	if (dir_y > 0)
		dir_y = 1;
	if (dir_y < 0)
		dir_y = -1;
	while (x < x1)
	{
		mlx_pixel_put(mlx_ptr, win_ptr, x, y, 0xFFFFFF);
		error = error + delta_error;
		if (2 * error >= delta_x)
		{
			y = y + dir_y;
			error = error - delta_x;
		}
		x++;
	}
}

void	ft_draw(void *mlx_ptr, void *win_ptr, t_point **map, int size_x, int size_y)
{
	int x;
	int y;

	x = 0;
	y = 0;
	while (y < size_y - 1)
	{
		while (x < size_x - 1)
		{
			ft_draw_line(map[x][y].x, map[x + 1][y].x, map[x][y].y, map[x][y].y, mlx_ptr, win_ptr);
			x++;
		}
		x = 0;
		y++;
	}
}

int		ft_pixelput(t_point **map, int size_x, int size_y)
{
	void	*mlx_ptr;
	void	*win_ptr;

	mlx_ptr = mlx_init();
	win_ptr = mlx_new_window(mlx_ptr, 1000, 1000, "Window");
	ft_draw(mlx_ptr, win_ptr, map, size_x, size_y);
	mlx_loop(mlx_ptr);
	return (1);
}
