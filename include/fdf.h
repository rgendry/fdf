/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgendry <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/27 19:48:43 by rgendry           #+#    #+#             */
/*   Updated: 2019/04/11 17:19:07 by rgendry          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# include <fcntl.h>
# include "libft.h"
# include "mlx.h"
# include "math.h"

typedef struct s_point
{
    int         x;
    int         y;
    int         z;
    //int         color;
}               t_point;

int             ft_validation(char *file);
int             ft_pixelput(t_point **map, int size_x, int size_y);

#endif